import Vue from 'vue';
import App from './App.vue'
import Buefy from 'buefy'
import VueRouter from 'vue-router'
import 'buefy/dist/buefy.css'

import routes from './routes'
Vue.use(Buefy)
Vue.use(VueRouter)

import Mindmap from 'vue-mindmap'
import 'vue-mindmap/dist/vue-mindmap.css'
Vue.use(Mindmap)

Vue.config.productionTip = false

const router = new VueRouter({routes,
                              scrollBehavior (to, from, savedPosition) {
                                if (savedPosition) {
                                  return savedPosition
                                } else {
                                  return { x: 0, y: 0 }
                                }
                              }
                            });
  
new Vue({
  router,
  render: h => h(App),
}).$mount('#app');