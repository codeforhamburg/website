export default{
  nodes:[
     {
        "text":"CodeForAll",
        "category":"Netzwerk",
        "url":"https://codeforall.org/",
        "note":"",
        "fx":-26.757400512695312,
        "fy":-514.5350782609955,
        "width":135,
        "height":63,
        "nodesWidth":0,
        "nodesHeight":0,
        "index":0,
        "x":-26.757400512695312,
        "y":-514.5350782609955,
        "vy":0,
        "vx":0
     },
     {
        "text":"CodeForGermany",
        "category":"Netzwerk",
        "url":"https://codefor.de/",
        "note":"",
        "width":189,
        "height":63,
        "nodesWidth":0,
        "nodesHeight":0,
        "index":1,
        "fx":-200.5351848602295,
        "fy":-365.4754638671875,
        "x":-200.5351848602295,
        "y":-365.4754638671875,
        "vy":0,
        "vx":0
     },
     {
        "text":"CodeForHamburg",
        "category":"Netzwerk",
        "url":"https://codeforhamburg.org/",
        "note":"Das sind wir",
        "width":190,
        "height":63,
        "nodesWidth":0,
        "nodesHeight":0,
        "index":2,
        "fx":100,
        "fy":-290,
        "x":100,
        "y":-290,
        "vy":0,
        "vx":0
     },
     {
        "text":"Projekte",
        "category":"Projekte",
        "note":"",
        "nodes":[
           {
              "text":"Forum Offene Stadt",
              "url":"https://offenestadt.info/"
           },
           {
              "text":"luftdaten.info",
              "url":"https://luftdaten.info/"
           },
           {
              "text":"Klimawatch",
              "url":"https://klimawatch.codefor.de/kommunen/hamburg/"
           }
        ],
        "width":103,
        "height":63,
        "nodesWidth":167,
        "nodesHeight":114,
        "index":3,
        "fx":-137.95191192626953,
        "fy":-33.17005157470703,
        "x":-137.95191192626953,
        "y":-33.17005157470703,
        "vy":0,
        "vx":0
     },
     {
        "text":"Freunde",
        "category":"Freunde",
        "note":"",
        "nodes":[
           {
              "text":"Jugendhackt Nord",
              "url":"https://jugendhackt.org/"
           },
           {
              "text":"FragDenStaat.de",
              "url":"https://fragdenstaat.de/"
           },
           {
              "text":"Schülerforschungszentrum",
              "url":"https://www.sfz-hamburg.de/"
           },
           {
              "text":"GovData",
              "url":"https://www.govdata.de/"
            },
            {
              "text":"Urban Data Hub",
              "url":"https://www.hamburg.de/bsw/urban-data-hub/"
            },
           {
              "text":"Aktivoli",
              "url":"https://www.aktivoli.de/"
           },
           {
              "text":"Transparenzportal Hamburg",
              "url":"http://transparenz.hamburg.de/"
           },
           {
              "text":"Betahaus Hamburg",
              "url":"https://hamburg.betahaus.de/"
           },
           {
              "text":"CodeWeek",
              "url":"https://hamburg.codeweek.de/"
           },
           {
              "text":"Balloop",
              "url":"https://balloop.art/"
           },
           {
            "text":"Wikipedia Hamburg",
            "url":"https://de.wikipedia.org/wiki/Wikipedia:Kontor_Hamburg"
           },
        ],
        "width":102,
        "height":63,
        "nodesWidth":219,
        "nodesHeight":380,
        "index":4,
        "fx":159.5986328125,
        "fy":-125.5659408569336,
        "x":159.5986328125,
        "y":-125.5659408569336,
        "vy":0,
        "vx":0
     },
     {
        "text":"Unterstützer",
        "category":"Unterstützer",
        "note":"",
        "nodes":[
           {
              "text":"We-Build.city"
           },
           {
              "text":"eos-uptrade.de"
           }
        ],
        "width":123,
        "height":63,
        "nodesWidth":133,
        "nodesHeight":76,
        "index":5,
        "fx":175.64812833191502,
        "fy":-416.4570007324219,
        "x":175.64812833191502,
        "y":-416.4570007324219,
        "vy":0,
        "vx":0
     },
     {
        "text":"Partner",
        "category":"Partner",
        "note":"",
        "nodes":[
           {
              "text":"Körber Stiftung"
           }
        ],
        "width":94,
        "height":63,
        "nodesWidth":140,
        "nodesHeight":38,
        "index":6,
        "fx":-240.26091146469116,
        "fy":-158.16365814208984,
        "x":-240.26091146469116,
        "y":-158.16365814208984,
        "vy":0,
        "vx":0
     }
  ],
  connections:[
     {
        "source":"CodeForAll",
        "target":"CodeForGermany",
     },
     {
        "source":"CodeForGermany",
        "target":"CodeForHamburg",
     },
     {
        "source":"CodeForGermany",
        "target":"CodeForHamburg",
     },
     {
        "source":"CodeForHamburg",
        "target":"Projekte",
     },
     {
        "source":"CodeForHamburg",
        "target":"Freunde",
     },
     {
        "source":"CodeForHamburg",
        "target":"Unterstützer",
     },
     {
        "source":"CodeForHamburg",
        "target":"Partner",
     }
  ]
}