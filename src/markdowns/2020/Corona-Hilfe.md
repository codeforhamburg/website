## Code for Hamburg hilft den Helfer*innen!

Durch Corona arbeiten gerade viele gemeinnützige Organisationen und Vereine im Homeoffice - doch nicht immer sind ihre realen Arbeitsbedingungen auf diese kurzfristige Umgestaltung ausgelegt. Gleichzeitig entsteht aber besonders im sozialen Bereich oftmals akuter Handlungsbedarf, weshalb schnelle und unbürokratische Hilfe erforderlich ist.

Darum bietet Code for Hamburg allen gemeinnützigen Vereinen und Organisationen in und um Hamburg konkrete Hilfe an. Zum Beispiel hinsichtlich der Beratung und der Einrichtung von:

* Tools für Videokonferenzen und Online Meetings
* oder Tools zur gemeinsamen Dokumentpflege.

**Hilfe erhalten:**
Wenn ihr ein gemeinnütziger Vereinen oder Organisationen seid und Fragen habt bzw. Unterstützung braucht, meldet Euch gerne per Mail mit dem Betreff [Suche](mailto:info@codeforhamburg.org?subject=Suche) an uns.

**Hilfe anbieten:**
Du hast ein gutes Verständnis für IT und traust Dir zu, das Code for Hamburg Team beim oben genannten Angebot zu unterstützen? Super! Melde Dich gerne per Mail mit dem Betreff [Biete](mailto:info@codeforhamburg.org?subject=Biete) an uns. 

Da wir bei Code for Hamburg ehrenamtlich tätig und unsere Ressourcen begrenzt sind, versuchen wir das Netz an Helfer*innen stetig zu erweitern, um Hilfebietende und Hilfesuchende so schnell wie möglich miteinander zu vernetzen. 

Wir freuen uns auf Eure Nachrichten! 

Euer Code for Hamburg-Team