
## CfG – Eine Community trifft sich

Zum ersten Mal seit seiner Gründung vor rund fünf Jahren haben sich die Code for Germany-Labs (CfG) zum ersten Community-Event in Hamburg getroffen. In der Vergangenheit gab es immer wieder verschiedene Events in denen einige CfG-Labs vor Ort waren, aber ein Wochenende, wo die CfG-Labs im Fokus und deren Weiterentwicklung standen, gab es bisher nicht.

Auf Anregung des CfG-Lab Hamburg (Code for Hamburg) fand am zweiten Novemberwochenende ein Treffen von rund 60 Menschen aus den 14 von 25 deutschlandweiten Labs statt. Einen Tag zuvor trafen sich bereits viele Teilnehmer\*innen beim Forum Offene Stadt, in der Körber-Stiftung. Diese Veranstaltung verband die Zivilgesellschaft, die Verwaltung und die Unternehmen mit dem Fokus, wie offene digitale Lösungen dabei helfen können, die Stadt und Region mitzugestalten. Daher war es naheliegend diese beiden Termine gemeinsam in Hamburg zu verbinden, getreu dem Motto von Hamburg: „Das Tor zur Welt“.

In einer ehemaligen Fabrikhalle im Herzen der Sternschanze fanden die Teilnehmer\*innen die Voraussetzungen für ein gutes produktives Wochenende: Räume, Tische, Stühle, Strom und WLAN. Leider gab es einige Probleme mit der Heizung und so war es zwischenzeitlich etwas frisch in den Räumlichkeiten. Inhaltlich ging es um Fragen der Infrastruktur, wie die verschiedenen Dienste, die zur Vernetzung und Kommunikation genutzt werden, aber auch welche Vorteile- und Nachteile die Gründung von eigenen Vereinen hat. Auch die Mitbestimmung innerhalb der Community und die Beteiligungsformate wurden erörtert.

In mehreren Sessions wurden auch Themen und Techniken besprochen wie LoRaWan, Open Data in Kommunen, Musterdatenkataloge, Infrastrukturen fürs digitale Ehrenamt und Planungen für 2020 aufgenommen. Für 2020 ist ein Projekt in Planung, an denen sich alle Labs beteiligen sollen. Bereits acht Events sind in Planung an denen mehrere CfG-Labs teilnehmen werden.

Ein gemeinsames Anliegen aller CfG-Labs ist die Suche nach neuen Mitgliedern, weshalb sich über verschiedene Formate und Angebote ausgetauscht wurde, um das Engagement zu fördern.

Zukünftig soll das CfG Community Summit jährlich stattfinden und hierbei die Plattform für den Austausch und Innovationen der Open Knowledge & Open Source Community bilden. Im Vordergrund steht der Austausch der ehrenamtlich tätigen Innovator\*innen, Wissenschaftler\*innen, Entwickler\*innen, Policy-Spezialist\*innen und Designer\*innen aus den verschiedenen Städten in ganz Deutschland. Dabei geht es um Weiterentwicklung der gemeinsamen Ideen und Ziele rund um offene Daten.
<br>
<br>
Autor: Christopher Bohlens