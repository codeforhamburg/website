
## neuen Blog Eintrag anlegen

#1 Neuen Blog Eintrag im Markdown Format in src/markdown/ schreiben
#2 Eintrag in src/data/blogs.json hinzufügen
#3 commiten und pushen

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
